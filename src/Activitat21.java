import java.util.Arrays;

public class Activitat21 {
    public static void main(String[] args) {
        String[] nombres = {"Carolina", "Iván", "Blas",
                "Izan", "Germán", "Cristian",
                "Jose Enrique", "Ismael",
                "Diego", "Joan David", "Juan Carlos",
                "Toni", "Pablo"};
        System.out.println(Arrays.toString(nombres));
        ordenarPorSeleccion(nombres);
        System.out.println(Arrays.toString(nombres));
    }

    public static void ordenarPorSeleccion(String[] array) {
          for (int i = 0; i < array.length; i++) {
              int indiceElementoMenor = i;
              for (int j = i+1; j < array.length; j++) {
                  if (array[j].compareTo(array[indiceElementoMenor]) < 0) {
                      indiceElementoMenor = j;
                  }
              }
              if (indiceElementoMenor != i) {
                  String aux = array[indiceElementoMenor];
                  array[indiceElementoMenor] = array[i];
                  array[i] = aux;
              }
          }
    }
}
