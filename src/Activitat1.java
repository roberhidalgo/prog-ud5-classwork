public class Activitat1 {

    public static final String SALUDO_1 = "hola";
    public static final String SALUDO_2 = "hello";
    public static final String SALUDO_3 = "Què tal";

    public static void main(String[] args) {
        System.out.println(esUnSaludo("Adiós"));
        System.out.println(esUnSaludo("hola"));
        System.out.println(esUnSaludo("QUÈ TAL"));
    }

    public static boolean esUnSaludo(String cadena) {
        if (SALUDO_1.equalsIgnoreCase(cadena)
            || SALUDO_2.equalsIgnoreCase(cadena)
            || SALUDO_3.equalsIgnoreCase(cadena)) {
            return true;
        }

        return false;
    }
}
