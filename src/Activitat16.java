import java.util.Random;

public class Activitat16 {

    static final int TAMANYO = 200;

    public static void main(String[] args) {
        int[] numeros = new int[TAMANYO];
        Random random = new Random();
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = random.nextInt(TAMANYO+1); 
        }

        int suma = 0;
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] % 2 != 0) {
                suma += numeros[i];
            }
        }
        System.out.println("La suma es " + suma);
    }

}
