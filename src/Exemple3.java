public class Exemple3 {

    public static void main (String [] args) {
        StringBuilder a = new StringBuilder ("Hola Món");
        System.out.println ("Abans de la cridada: a = " + a);
        canviarValor(a);
        System.out.println ("Després de la cridada: a = " + a);
        // Després de la cridada: a = Hola Món Java
    }

    public static void canviarValor (StringBuilder x) {
        x.append(" Java");
        System.out.println ("Dins del mètode: x = " + x);
    }

}
