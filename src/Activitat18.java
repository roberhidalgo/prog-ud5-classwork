import java.util.Arrays;

public class Activitat18 {

    public static String[] copiaArray(String[] array) {
        String[] copia = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            copia[i] = array[i];
        }

        return copia;
    }

    public static void main(String[] args) {
        String[] diesDeLaSetmana = {"Dilluns", "Dimarts", "Dimecres"};
        String[] diesDeLaSetmana2 = diesDeLaSetmana;
        diesDeLaSetmana2[0] = "Monday";

        System.out.println(Arrays.toString(diesDeLaSetmana));
        System.out.println(Arrays.toString(diesDeLaSetmana2));

        String[] diesDeLaSetmana3 = copiaArray(diesDeLaSetmana);
        diesDeLaSetmana3[0] = "Lunes";

        System.out.println(Arrays.toString(diesDeLaSetmana));
        System.out.println(Arrays.toString(diesDeLaSetmana2));
        System.out.println(Arrays.toString(diesDeLaSetmana3));

    }

}
