import java.util.Arrays;
import java.util.Random;

public class Activitat17 {
    static final int TAMANYO_ARRAY = 10;
    static final int VALOR_MAXIMO = 50;

    public static void main(String[] args) {
        int[] array = crearArray();
        visualitzaArray(array);
        int indice = cercarZero(array);
        if (indice != -1) {
            System.out.println("El 0 está en " + indice);
        } else {
            System.out.println("No hay 0");
        }
        intercanvia(array);
        visualitzaArray(array);

    }

    public static int[] crearArray() {
        Random random = new Random();
        int[] numeros = new int[TAMANYO_ARRAY];
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = random.nextInt(VALOR_MAXIMO+1);
        }
        return numeros;
    }

    public static void visualitzaArray(int[] vector) {
        for (int vectorI: vector) {
            System.out.print(vectorI + " ");
        }
        System.out.println();
    }

    public static int cercarZero(int [] vector) {
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] == 0) {
                return i;
            }
        }

        return -1;
    }

    public static void intercanvia(int [] vector) {
        if (vector.length >= 2) {
            int aux = vector[vector.length-1];
            vector[vector.length-1] = vector[0];
            vector[0] = aux;
        }
    }
}
