public class Activitat2 {

    public static void main(String[] args) {
        mostrarCadenaConGuiones("patata");
        mostrarCadenaConGuiones("");
        mostrarCadenaConGuiones("12 345");
    }
    public static void mostrarCadenaConGuiones(String cadena) {
        for (int i = 0; i < cadena.length(); i++) {
            System.out.print(cadena.charAt(i));
            if (i < cadena.length()-1) {
                System.out.print("-");
            }

        }
        System.out.println();
    }
}
