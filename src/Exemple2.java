public class Exemple2 {

    public static void main (String [] args) {
        String a = "Hola Món";
        System.out.println ("Abans de la cridada: a = " + a);
        canviarValor(a);
        System.out.println ("Després de la cridada: a = " + a);
        // Després de la cridada: a = Hola Món
    }

    public static void canviarValor (String x) {
        x = x + " Java";
        System.out.println ("Dins del mètode: x = " + x);
    }

}
