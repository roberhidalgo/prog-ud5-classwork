public class Exemple1 {

    public static void main(String[] args) {
        int a = 3;
        System.out.println("Abans de la cridada: a = " + a);
        canviarValor(a);
        System.out.println("Després de la cridada: a = " + a);
        // Després de la cridada: a = 3
    }

    public static void canviarValor(int x) {
        x = x + 3;
        System.out.println("Dins del mètode: x = " + x);
    }

}

