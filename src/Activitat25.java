import javax.crypto.spec.PSource;
import java.util.Arrays;
import java.util.Random;

public class Activitat25 {

    static final int NUM_ALEATORIOS = 9;

    public static void main(String[] args) {

        int[][] matriz = crearMatrizInicializada();

        // Punto 1
        System.out.println("Punto 1: Matriz entera\n");
        imprimirMatriz(matriz);
        System.out.println();
        // Punto 2
        System.out.println("Punto 2: Fila índice 4 entera\n");
        System.out.println(Arrays.toString(matriz[4]));
        System.out.println();

        // Punto 3
        System.out.println("Punto 3: Intercambiar elemento 3-2 y elemento 4-2\n");
        System.out.println("Antes");
        mostrarElemento(matriz, 3, 2);
        mostrarElemento(matriz, 4, 2);
        swapCeldas(matriz, 3, 2, 4, 2);
        System.out.println("Después");
        mostrarElemento(matriz, 3, 2);
        mostrarElemento(matriz, 4, 2);
        System.out.println();

        // Punto 4
        System.out.println("Punto 4: Intercambiar columna 0 con columna 3\n");
        mostrarColumna(matriz,0);
        mostrarColumna(matriz,3);
        for (int i = 0; i < matriz.length; i++) {
            swapCeldas(matriz, i, 0, i, 3);
        }
        System.out.println();
        imprimirMatriz(matriz);
    }

    /**
     * Intercambia dos celdas de una matriz
     * @param matriz Matriz implicada
     * @param fila1 fila de la celda 1
     * @param columna1 columna de la celda 1
     * @param fila2 fila de la celda 2
     * @param columna2 columna de la celda 2
     */
    public static void swapCeldas(
            int[][] matriz,
            int fila1, int columna1,
            int fila2, int columna2) {
        int aux = matriz[fila1][columna1];
        matriz[fila1][columna1] = matriz[fila2][columna2];
        matriz[fila2][columna2] = aux;
    }

    /**
     * Muestra por pantalla la columna de la matriz según el índice de columna proporcionada
     * @param matriz
     * @param indiceColumna
     */
    public static void mostrarColumna(int[][] matriz, int indiceColumna) {
        System.out.println("Columna índice " + indiceColumna);
        for (int i = 0; i < matriz.length; i++) {
            System.out.print(matriz[i][indiceColumna] + " ");
        }
        System.out.println();
    }

    /**
     * Muestra el contenido de una celda de una matriz
     * @param matriz
     * @param fila
     * @param columna
     */
    public static void mostrarElemento(int[][] matriz, int fila, int columna)  {
        System.out.println("Elemento [" + fila + "]["+ columna + "]:"
                + matriz[fila][columna]);
    }

    /**
     * Crea una matriz de 5 x 7 inicializada a números aleatorios
     * @return
     */

    public static int[][] crearMatrizInicializada() {
        Random random = new Random();
        int matriz[][] = new int[5][7];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                matriz[i][j] = random.nextInt(NUM_ALEATORIOS + 1);
            }
        }
        return matriz;
    }

    /**
     * Imprime una matriz de enteros
     * @param matriz
     */
    public static void imprimirMatriz(int[][] matriz) {

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + " ");
           }

            System.out.println();
        }
    }
}
