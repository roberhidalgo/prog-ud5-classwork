import java.util.Scanner;

public class Activitat5 {

    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);

        int num1 = Integer.valueOf(teclat.next());
        int num2 = Integer.valueOf(teclat.next());

        System.out.println("Suma:" + (num1+num2));
        System.out.println("Mult:" + num1*num2);

    }
}
