import java.util.Scanner;

public class Activitat14 {
    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);

        int[] numeros = new int[10];
        /*for (int i = 0; i < numeros.length; i++) {
            System.out.print("Número " + (i + 1) + ": ");
            numeros[i] = teclado.nextInt();
        }*/
        int i = 0;
        for (int numeroI : numeros) {
            System.out.print("Número " + (i + 1) + ": ");
            numeros[i] = teclado.nextInt();
            i++;
        }

        System.out.println();

        for (int numeroI : numeros) {
            System.out.println(numeroI);
        }
    }
}
