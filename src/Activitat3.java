public class Activitat3 {

    public static void main(String[] args) {
        System.out.println(obtenerMitadCadena("Hola mundo"));
        System.out.println(obtenerMitadCadena("Adiós"));
    }
    public static String obtenerMitadCadena(String cadena) {
        return cadena.substring(0, cadena.length()/2);
    }
}
