import java.util.Scanner;

public class Activitat6 {

    public static void main(String[] args) {
        String nombreCompleto = obtenerNombreCompleto();
        mostrarMinusMayusYLongitud(nombreCompleto);
        mostrarOcurrenciasUltimoCaracter(nombreCompleto);
        mostrar2PrimerosCaracteres(nombreCompleto);
        mostrarOcurrenciasPrimerCaracterMayusculas(nombreCompleto);
        mostrarConAsteriscos(nombreCompleto);
        mostrarInvertida(nombreCompleto);
    }

    public static String obtenerNombreCompleto() {
        Scanner teclado = new Scanner(System.in);

        String nombre = teclado.next();
        String apellido1 = teclado.next();
        String apellido2 = teclado.next();

        return nombre + " " + apellido1 + " " + apellido2;
    }

    public static void mostrarMinusMayusYLongitud(String nombreCompleto) {
        System.out.println("Minúsculas: " + nombreCompleto.toLowerCase());
        System.out.println("Mayúsculas: " + nombreCompleto.toUpperCase());
        System.out.println("Longitud: " + nombreCompleto.length());
    }

    public static void mostrar2PrimerosCaracteres(String nombreCompleto) {
        int numCaracteres = 2;
        if (nombreCompleto.length() >= numCaracteres)
            System.out.println("2 primeros caracteres:" + nombreCompleto.substring(0,numCaracteres));
    }

    public static void mostrarOcurrenciasUltimoCaracter(String nombreCompleto) {
        int longitudNombreCompleto = nombreCompleto.length();
        char ultimoCaracter = nombreCompleto.charAt(longitudNombreCompleto-1);
        int ocurrencias = 0;

        for (int i = 0; i < longitudNombreCompleto; i++) {
            if (nombreCompleto.charAt(i) == ultimoCaracter) {
                ocurrencias++;
            }
        }

        System.out.println("Número de ocurrencias de " + ultimoCaracter + " es " + ocurrencias);
    }

    public static void mostrarOcurrenciasPrimerCaracterMayusculas(String nombreCompleto) {
        if (nombreCompleto.length() > 0){
            char primerCaracter = nombreCompleto.charAt(0);
            System.out.println(
                nombreCompleto.replace(
                    String.valueOf(primerCaracter), String.valueOf(primerCaracter).toUpperCase()));
        }
    }

    public static void mostrarConAsteriscos(String nombreCompleto) {
        String asteriscos = "***";
        System.out.println(asteriscos.concat(nombreCompleto).concat(asteriscos));
    }

    public static void mostrarInvertida(String nombreCompleto) {
        System.out.print(obtenerInvertida(nombreCompleto));

    }

    public static String obtenerInvertida(String cadena) {
        String cadenaInvertida="";
        for (int i = cadena.length()-1; i >= 0; i--) {
            cadenaInvertida +=cadena.charAt(i);
        }

        return cadenaInvertida;
    }
}


