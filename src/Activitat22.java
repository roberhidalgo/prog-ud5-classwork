public class Activitat22 {

    public static void main(String[] args) {
        int [] numeros = Activitat20.obtenerArrayAleatorios();
        System.out.println(buscarEnVectorNoOrdenado(numeros, 14));
    }




















    public static int buscarEnVectorNoOrdenado(int[] numeros, int numBuscar) {
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == numBuscar)
                return i;
        }

        return -1;
    }

    public static int buscarEnVectorOrdenado(int[] numeros, int numBuscar) {
        Activitat20.ordenarPorIntercanvi(numeros);
        return buscarEnVectorNoOrdenado(numeros, numBuscar);
    }
}
