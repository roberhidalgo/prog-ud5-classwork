import java.util.Random;

public class Activitat15 {
    static final int TAMANYO = 20;
    static final int VALOR_MAXIMO = 9;
    static final int NUM_VALORES_JUNTOS = 4;
    public static void main(String[] args) {

        int numeros[] = new int[TAMANYO];

        for (int i = 0; i < numeros.length; i++) {
            Random randomizer = new Random();
            numeros[i] = randomizer.nextInt(VALOR_MAXIMO+1);
        }

        int contador = 0;
        for (int numeroI:numeros) {
            System.out.print(numeroI);
            contador++;
            if (contador % NUM_VALORES_JUNTOS == 0)
                System.out.print(" ");
        }
    }
}
