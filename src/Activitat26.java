public class Activitat26 {

    private static final int NOM = 0;
    private static final int COGNOMS = 1;
    private static final int EDAT = 2;
    private static final int CICLE = 3;
    private static final int CURS = 4;

    public static void main(String[] args) {
        String [][] tablaAlumnos = creaTablaInicializada();

        System.out.println("Punto 1");
        mostrarTablaCompleta(tablaAlumnos);
        System.out.println();

        System.out.println("Punto 2");
        mostrarInformacionDAWASIX(tablaAlumnos);
        System.out.println();

        System.out.println("Punto 3");
        mostrarMediaEdadAlumnos(tablaAlumnos);
        System.out.println();

        System.out.println("Punto 4");
        promocionarAlumnosDePrimero(tablaAlumnos);
        mostrarTablaCompleta(tablaAlumnos);
        System.out.println();

        System.out.println("Punto 5");
        mostrarTablaCompleta(creaTablaTotalAlumnosPorCiclo(tablaAlumnos));
        System.out.println();
    }
    public static String[][] creaTablaInicializada() {
        return new String[][]{
                {"Joan", "Pérez Aura", "24", "ASIX", "1"},
                {"Maria", "Sánchez García", "18", "DAW", "1"},
                {"Pepa", "Egea Juan", "21", "DAM", "1"},
                {"Ana María", "Hernández Julián", "20", "DAW", "2"},
                {"Francesc", "Juan Juan", "28", "DAW", "1"}
        };
    }

    public static void mostrarTablaCompleta(String[][] tabla) {

        for (int i = 0; i < tabla.length; i++) {
            mostrarInformacionAlumno(tabla[i]);
            System.out.println();
        }
    }

    public static void mostrarInformacionAlumno(String[] infoAlumno) {
        for (int i = 0; i < infoAlumno.length; i++) {
            System.out.print(infoAlumno[i] + "\t\t");
        }
    }

    public static void mostrarInformacionDAWASIX(String[][] tabla) {
        for (int i = 0; i < tabla.length; i++) {
            if (tabla[i][CICLE].equals("DAW")
                    || tabla[i][CICLE].equals("ASIX")) {
                mostrarInformacionAlumno(tabla[i]);
                System.out.println();
            }
        }
    }

    public static int mediaEdadAlumnos(String [][] tabla) {
        int total = 0;
        for (int i = 0; i < tabla.length; i++) {
            total += Integer.parseInt(tabla[i][EDAT]);
        }

        return total / tabla.length;
    }

    public static void mostrarMediaEdadAlumnos(String [][]tabla) {
        System.out.println("La media de edad es " + mediaEdadAlumnos(tabla));
    }

    public static void promocionarAlumnosDePrimero(String [][] tabla) {
        for (int i = 0; i < tabla.length; i++) {
            if (tabla[i][CURS].equals("1")){
                tabla[i][CURS] = String.valueOf(Integer.parseInt(tabla[i][CURS]) + 1);
            }
        }
    }

    public static String[][] creaTablaTotalAlumnosPorCiclo(String[][] tabla) {

        String[][] totalAlumnosPorCiclo = {
                {"DAW", "0"},
                {"DAM", "0"},
                {"ASIX", "0"}};

        for (int i = 0; i < tabla.length; i++) {
                switch (tabla[i][CICLE]) {
                    case "DAW" :
                        incrementaValorEnCelda(totalAlumnosPorCiclo[0]);
                        break;
                    case "DAM" :
                        incrementaValorEnCelda(totalAlumnosPorCiclo[1]);
                        break;
                    case "ASIX":
                        incrementaValorEnCelda(totalAlumnosPorCiclo[2]);
                        break;
                    default:
                        System.out.println("Error");
                }
        }

        return totalAlumnosPorCiclo;
    }

    public static void incrementaValorEnCelda(String[]numMatriculadosEnCiclo) {
        numMatriculadosEnCiclo[1] = String.valueOf(Integer.parseInt(numMatriculadosEnCiclo[1]) + 1);
    }
}
