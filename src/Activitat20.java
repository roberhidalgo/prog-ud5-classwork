import java.util.Arrays;
import java.util.Random;

public class Activitat20 {

    private static final int NUM_ALEATORIOS = 10;

    public static void main(String[] args) {

        int [] numeros1 = obtenerArrayAleatorios();
        muestraArraySinOrdenarYOrdenado(numeros1);

        int [] numeros2 = obtenerArrayAleatorios();
        muestraArraySinOrdenarYOrdenado(numeros2);
    }

    public static void muestraArraySinOrdenarYOrdenado(int[] numeros) {
        System.out.println("Sin ordenar: " + Arrays.toString(numeros));
        ordenarPorIntercanvi(numeros);
        System.out.println("Ordenado: " + Arrays.toString(numeros));
    }

    public static int[] obtenerArrayAleatorios() {
        Random random = new Random();
        int[] numeros = new int[NUM_ALEATORIOS];
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = random.nextInt(NUM_ALEATORIOS+1);
        }
        return numeros;
    }
    public static void ordenarPorIntercanvi(int[] array) {

        for (int i = 0; i < array.length; i++) {
            for (int j = i+1; j < array.length; j++) {
                if (array[j] < array[i]) {
                    int aux = array[i];
                    array[i] = array[j];
                    array[j] = aux;
                }
            }
        }
    }

}
