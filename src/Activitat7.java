public class Activitat7 {

    public static void main(String[] args) {
        System.out.println(esCapicua("somos"));
        System.out.println(esCapicua("capicua"));
        System.out.println(esCapicua("radar"));
    }
    public static boolean esCapicua(String cadena) {
        return Activitat6.obtenerInvertida(cadena).equals(cadena);
    }
}
