public class Exemple22 {
    public static void main(String[] args) {

        String cadena1 = "Hola Món";
        String cadena2 = "Hola Món2";
        String cadena3 = cadena2;
        String cadena4 = "Hola Món";

        if (cadena1 == cadena2) {
            System.out.println("1." + cadena1 + "==" + cadena2);
        }
        if (cadena2 == cadena3) {
            System.out.println("2." + cadena2 + "==" + cadena3);
        }
        if (cadena1.equalsIgnoreCase(cadena2)) {
            System.out.println("3." + cadena1 + ".equalsIgnoreCase(" + cadena3 + ")");
        }
        if (cadena1.equalsIgnoreCase(cadena4)) {
            System.out.println("4." + cadena1 + ".equalsIgnoreCase(" + cadena4 + ")");
        }
        if (cadena1 == cadena4) {
            System.out.println("5." + cadena1 + "==" + cadena4);
        }

        String cadena5 = "patata";
        String cadena6 = "pepino";

        System.out.println("CompareTo(patata vs pepino):" + cadena5.compareTo(cadena6));
        System.out.println("CompareTo(Hola Món vs patata):"
                + cadena1.compareTo(cadena5));

        System.out.println("Vull dir-te que no vull que te'n vagis".indexOf("que"));

    }

}
