public class Activitat4 {

    public static void main(String[] args) {
        System.out.println(cambiarMinusculasPorA("botella"));
        System.out.println(cambiarMinusculasPorA("bOtEllA"));
        System.out.println(cambiarMinusculasPorA(""));
        System.out.println(cambiarMinusculasPorA("BOTELLA"));
    }

    public static String cambiarMinusculasPorA(String cadena) {

        char vocalSustituta = 'a';

        String cadenaFinal = cadena.replace('e', vocalSustituta);
        cadenaFinal = cadenaFinal.replace('i',vocalSustituta);
        cadenaFinal = cadenaFinal.replace('o',vocalSustituta);
        return cadenaFinal.replace('u',vocalSustituta);
    }
}
