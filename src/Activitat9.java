public class Activitat9 {
    public static void main(String[] args) {

        System.out.println(obtenerNumCaracsEspeciales("hola! mi mail es rob@rob.com"));
        System.out.println(obtenerNumCaracsEspeciales("#prueba => [correcta?]"));
    }

    public static int obtenerNumCaracsEspeciales(String cadena) {
        int numCaracteresEspeciales = 0;
        for (int i = 0; i < cadena.length(); i++) {
            char caracterI = cadena.charAt(i);
            if (estaEnRango(caracterI, '\u0021', '\u002F')
                || estaEnRango(caracterI, '\u003A', '\u0040')
                || estaEnRango(caracterI, '\u005B', '\u005F')
                || estaEnRango(caracterI, '\u007B', '\u007D')
                || caracterI == ' ') {
                numCaracteresEspeciales++;
            }
        }

        return numCaracteresEspeciales;
    }

    public static boolean estaEnRango(
            char caracter, char limiteIzq, char limiteDer) {
        if (caracter >= limiteIzq && caracter <= limiteDer)
            return true;

        return false;
    }
}
