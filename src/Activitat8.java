import java.util.Scanner;

public class Activitat8 {

    public static void main(String[] args) {

        StringBuilder cadena = new StringBuilder("");
        Scanner teclado = new Scanner(System.in);

        cadena.append(teclado.next());
        cadena.append(" ");
        cadena.append(teclado.next());
        cadena.append(" ");
        cadena.append(teclado.next());

        System.out.println(cadena.reverse());
    }

}
